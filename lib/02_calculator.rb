def add(el1, el2)
  el1 + el2
end

def subtract(el1, el2)
  el1 - el2
end

def sum(array)
  array.reduce(0, :+)
end

def multiply(array)
  array.reduce(1, :*)
end

def power(el1, el2)
  el1**el2
end

def factorial(num)
  if num == 0
    return 0
  else
    (1..num).inject(1) {|sum, el| sum * el}
  end
end
