def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, ntimes = 2)
  ([word] * ntimes).join(' ')
end

def start_of_word(word, nletters = 1)
  word[0...nletters]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  ignore_words = ["and", "the", "is", "or", "of", "a", "over"]
  answer = string.split.map do |word|
    if ignore_words.include?(word)
      word
    else
      word.capitalize
    end
  end
  answer.first.capitalize!
  answer.join(" ")
end
