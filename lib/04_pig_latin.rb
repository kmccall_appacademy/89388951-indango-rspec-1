def translate(string)
  new_str  = string.split.map {|word| iggipayfied(word)}.join(" ")
end

def iggipayfied(word)
  if word == word.downcase
    return word_flip(word)
  else
    return word_flip(word).capitalize
  end
end

def word_flip(word)
  word_arr = word.downcase.chars
  new_word = []
  word_arr.each_index do |i|
    if "aeio".include?(word_arr[i])
      break new_word << word_arr[i..-1] << word_arr[0...i] << "ay"
    elsif "u".include?(word_arr[i])
      break new_word << word_arr[(i+1)..-1] << word_arr[0..i] << "ay"
    end
  end
  new_word.join
end
